﻿namespace Minesweeper
{
    public class MinesweeperModelConfig
    {
        public ILogger Logger;
        public ITime Time;
        public bool IsTestMode;
    }
}