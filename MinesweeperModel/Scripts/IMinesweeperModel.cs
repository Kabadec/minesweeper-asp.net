using Minesweeper.Minefield;
using Minesweeper.Minefield.Coordinates;
using Minesweeper.MinesweeperStates;

namespace Minesweeper
{
    public interface IMinesweeperModel
    {
        int Width { get; }
        int Height { get; }
        int NumOpenedCells { get; }
        int NumMines { get; }
        MatchState CurrentMatchState { get; }
        int NumMarks { get; }
        bool IsGameEnd { get; }
        DateTime TimeStartGame { get; }
        DateTime TimeEndGame { get; }
        void Configure(MinefieldConfig minefieldConfig);
        MinefieldState GetMinefieldState();
        CellState GetCellState(CellCoordinates coords);
        bool IsCellOpened(CellCoordinates coords);

        /// <summary>
        /// Открывает клетку, при условии, что клетка не была открыта и не помечена флажком
        /// </summary>
        MinefieldChangeArgs TryOpenCell(CellCoordinates coords);

        /// <summary>
        /// Открывает блок клеток размером 3х3, при условии, что центр блока находится на открытой клетке
        /// и вокруг центра блока количество помеченных клеток равно или больше, чем значение NumMinesAround центра блока
        /// </summary>
        MinefieldChangeArgs TryOpenBlock(CellCoordinates blockCenterCoords);

        MinefieldChangeArgs TrySwitchMark(CellCoordinates coords);
        void RestoreData(MinesweeperStoredData minesweeperStoredData);
        MinesweeperStoredData StoreData();
    }
}