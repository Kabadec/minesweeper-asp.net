﻿namespace Minesweeper.Minefield;

public class MinefieldConfig
{
    public int Width;
    public int Height;
    public HashSet<int> MinesMap;
}