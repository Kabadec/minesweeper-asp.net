﻿namespace Minesweeper.Minefield
{
    internal class CellConditionToStateConverter
    {
        private CellDefinition m_Definition;
        private CellCondition m_Condition;

        public CellState GetCellState(CellDefinition definition, CellCondition condition)
        {
            m_Condition = condition;
            m_Definition = definition;

            if (AllowMarked())
                return CellState.Marked;
            if (AllowIdleClosed())
                return CellState.IdleClosed;
            if (AllowMine())
                return CellState.Mine;
            if (AllowIdleOpened())
                return CellState.IdleOpened;
            if (AllowDigitOne())
                return CellState.DigitOne;
            if (AllowDigitTwo())
                return CellState.DigitTwo;
            if (AllowDigitThree())
                return CellState.DigitThree;
            if (AllowDigitFour())
                return CellState.DigitFour;
            if (AllowDigitFive())
                return CellState.DigitFive;
            if (AllowDigitSix())
                return CellState.DigitSix;
            if (AllowDigitSeven())
                return CellState.DigitSeven;
            if (AllowDigitEight())
                return CellState.DigitEight;

            return CellState.IdleClosed;
        }

        private bool AllowMarked()
        {
            return !m_Condition.IsOpen && m_Condition.IsMark;
        }

        private bool AllowIdleClosed()
        {
            return !m_Condition.IsOpen && !m_Condition.IsMark;
        }

        private bool AllowMine()
        {
            return m_Condition.IsOpen && m_Definition.IsMine;
        }

        private bool AllowIdleOpened()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround <= 0;
        }

        private bool AllowDigitOne()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 1;
        }

        private bool AllowDigitTwo()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 2;
        }

        private bool AllowDigitThree()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 3;
        }

        private bool AllowDigitFour()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 4;
        }

        private bool AllowDigitFive()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 5;
        }

        private bool AllowDigitSix()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 6;
        }

        private bool AllowDigitSeven()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 7;
        }

        private bool AllowDigitEight()
        {
            return m_Condition.IsOpen && !m_Definition.IsMine && m_Definition.NumMinesAround == 8;
        }
    }
}