namespace Minesweeper.Minefield
{
    internal struct CellCondition
    {
        public bool IsOpen;
        public bool IsMark;
    }
}