using Minesweeper.Minefield.Coordinates;

namespace Minesweeper.Minefield
{
    public struct CellChangeArgs
    {
        public CellCoordinates Coordinates;
        public CellState CellState;
    }
}