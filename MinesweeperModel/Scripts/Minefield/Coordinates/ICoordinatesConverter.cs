namespace Minesweeper.Minefield.Coordinates
{
    internal interface ICoordinatesConverter
    {
        int ConvertCoordinatesToIndex(CellCoordinates coords);
        CellCoordinates ConvertIndexToCoordinates(int index);
    }
}