﻿namespace Minesweeper.Minefield.Coordinates;

public class CoordinatesHandler : ICoordinatesConverter, ICoordinatesValidator
{
    private readonly int m_Width;
    private readonly int m_Height;

    public CoordinatesHandler(int width, int height)
    {
        m_Width = width;
        m_Height = height;
    }

    public int ConvertCoordinatesToIndex(CellCoordinates coords)
    {
        if (!ValidateCoordinates(coords))
        {
            return -1;
        }

        return coords.y * m_Width + coords.x;
    }

    public CellCoordinates ConvertIndexToCoordinates(int index)
    {
        return new CellCoordinates(index % m_Width, index / m_Width);
    }

    public bool ValidateIndex(int index)
    {
        return index >= 0 && index < m_Width * m_Height;
    }

    public bool ValidateCoordinates(CellCoordinates coords)
    {
        if (coords.x < 0 || coords.x >= m_Width || coords.y < 0 || coords.y >= m_Height)
        {
            return false;
        }

        return true;
    }
}