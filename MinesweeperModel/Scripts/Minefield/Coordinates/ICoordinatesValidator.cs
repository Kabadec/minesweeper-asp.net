﻿namespace Minesweeper.Minefield.Coordinates;

internal interface ICoordinatesValidator
{
    bool ValidateIndex(int index);
    bool ValidateCoordinates(CellCoordinates coords);
}