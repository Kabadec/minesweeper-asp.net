﻿namespace Minesweeper.Minefield.Coordinates
{
    public struct CellCoordinates
    {
        public int x;
        public int y;

        public CellCoordinates(int x, int y)
        {
            this.x = x;
            this.y = y;
        }

        public static CellCoordinates operator +(CellCoordinates a, CellCoordinates b)
        {
            return new CellCoordinates(a.x + b.x, a.y + b.y);
        }

        public static CellCoordinates operator -(CellCoordinates a, CellCoordinates b)
        {
            return new CellCoordinates(a.x - b.x, a.y - b.y);
        }

        public static CellCoordinates operator *(CellCoordinates a, int scalar)
        {
            return new CellCoordinates(a.x * scalar, a.y * scalar);
        }

        public static bool operator ==(CellCoordinates a, CellCoordinates b)
        {
            return a.x == b.x && a.y == b.y;
        }

        public static bool operator !=(CellCoordinates a, CellCoordinates b)
        {
            return !(a == b);
        }

        public override bool Equals(object obj)
        {
            if (!(obj is CellCoordinates))
                return false;

            CellCoordinates other = (CellCoordinates)obj;
            return this == other;
        }

        public override int GetHashCode()
        {
            return x.GetHashCode() ^ y.GetHashCode();
        }

        public override string ToString()
        {
            return $"({x}, {y})";
        }
    }
}