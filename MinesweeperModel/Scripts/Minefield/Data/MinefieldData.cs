﻿namespace Minesweeper.Minefield.Data
{
    internal class MinefieldData
    {
        private readonly int m_Width;
        private readonly int m_Height;
        private readonly HashSet<int> m_MinesMap;
        private readonly HashSet<int> m_OpenMap;
        private readonly HashSet<int> m_MarkMap;

        public int Width => m_Width;
        public int Height => m_Height;
        public HashSet<int> MinesMap => m_MinesMap;
        public HashSet<int> OpenMap => m_OpenMap;
        public HashSet<int> MarkMap => m_MarkMap;

        public MinefieldData(MinefieldDataConfig minefieldDataConfig)
        {
            m_Width = minefieldDataConfig.Width;
            m_Height = minefieldDataConfig.Height;
            m_MinesMap = minefieldDataConfig.MinesMap;
            m_OpenMap = minefieldDataConfig.OpenMap;
            m_MarkMap = minefieldDataConfig.MarkMap;
        }
    }
}