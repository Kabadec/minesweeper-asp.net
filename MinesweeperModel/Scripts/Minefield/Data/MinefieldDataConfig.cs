﻿namespace Minesweeper.Minefield.Data;

public class MinefieldDataConfig
{
    public int Width;
    public int Height;
    public HashSet<int> MinesMap;
    public HashSet<int> OpenMap;
    public HashSet<int> MarkMap;
}