﻿using Minesweeper.Minefield.Coordinates;

namespace Minesweeper.Minefield;

internal class AroundIndexCalculator
{
    private readonly ICoordinatesConverter m_CoordinatesConverter;
    private readonly ICoordinatesValidator m_CoordinatesValidator;

    public AroundIndexCalculator(ICoordinatesConverter coordinatesConverter, ICoordinatesValidator coordinatesValidator)
    {
        m_CoordinatesConverter = coordinatesConverter;
        m_CoordinatesValidator = coordinatesValidator;
    }

    public HashSet<int> GetAllAroundIndexes(int index)
    {
        var coords = m_CoordinatesConverter.ConvertIndexToCoordinates(index);
        return GetAllAroundIndexes(coords);
    }

    public HashSet<int> GetAllAroundIndexes(CellCoordinates coords)
    {
        var aroundIndexes = new HashSet<int>
        {
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x - 1, coords.y)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x - 1, coords.y - 1)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x - 1, coords.y + 1)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x, coords.y + 1)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x, coords.y - 1)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x + 1, coords.y)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x + 1, coords.y - 1)),
            m_CoordinatesConverter.ConvertCoordinatesToIndex(new CellCoordinates(coords.x + 1, coords.y + 1))
        };

        foreach (var aroundIndex in aroundIndexes.ToList())
        {
            if (!m_CoordinatesValidator.ValidateIndex(aroundIndex))
            {
                aroundIndexes.Remove(aroundIndex);
            }
        }

        aroundIndexes.Remove(-1);
        return aroundIndexes;
    }
}