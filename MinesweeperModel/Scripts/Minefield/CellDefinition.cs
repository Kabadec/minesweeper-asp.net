﻿namespace Minesweeper.Minefield
{
    internal struct CellDefinition
    {
        public bool IsMine;
        public int NumMinesAround;
    }
}