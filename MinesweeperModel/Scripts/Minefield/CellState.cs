﻿namespace Minesweeper.Minefield
{
    public enum CellState
    {
        IdleClosed = 0,
        IdleOpened = 1,
        Marked = 2,
        Mine = 3,
        ExplodedMine = 4,
        DigitOne = 5,
        DigitTwo = 6,
        DigitThree = 7,
        DigitFour = 8,
        DigitFive = 9,
        DigitSix = 10,
        DigitSeven = 11,
        DigitEight = 12
    }
}