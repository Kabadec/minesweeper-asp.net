using Minesweeper.MinesweeperStates;

namespace Minesweeper.Minefield
{
    public class MinefieldChangeArgs
    {
        public MatchState MatchState;
        public IReadOnlyList<CellChangeArgs> CellsChangeArgs;
    }
}