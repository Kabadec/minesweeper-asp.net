﻿using Minesweeper.Minefield.Coordinates;
using Minesweeper.Minefield.Data;

namespace Minesweeper.Minefield
{
    internal class MinefieldModel : ICellStateGetter
    {
        private readonly CellConditionToStateConverter m_CellConditionToStateConverter =
            new CellConditionToStateConverter();

        private MinefieldData m_Data;
        private AroundIndexCalculator m_AroundIndexCalculator;
        private CoordinatesHandler m_CoordinatesHandler;

        public int Width => m_Data.Width;
        public int Height => m_Data.Height;
        public int NumCells => m_Data.Width * m_Data.Height;
        public int NumMines => m_Data.MinesMap.Count;
        public int NumOpenedCells => m_Data.OpenMap.Count;
        public int NumMarks => m_Data.MarkMap.Count;
        public AroundIndexCalculator AroundIndexCalculator => m_AroundIndexCalculator;
        public ICoordinatesConverter CoordinatesConverter => m_CoordinatesHandler;
        public ICoordinatesValidator CoordinatesValidator => m_CoordinatesHandler;

        public MinefieldModel()
        {
            m_Data = new MinefieldData(new MinefieldDataConfig()
            {
                Width = 0,
                Height = 0,
                MinesMap = new HashSet<int>(),
                OpenMap = new HashSet<int>(),
                MarkMap = new HashSet<int>()
            });

            m_CoordinatesHandler = new CoordinatesHandler(0, 0);
            m_AroundIndexCalculator = new AroundIndexCalculator(m_CoordinatesHandler, m_CoordinatesHandler);
        }

        public void Configure(MinefieldConfig minefieldConfig)
        {
            m_Data = new MinefieldData(new MinefieldDataConfig()
            {
                Width = minefieldConfig.Width,
                Height = minefieldConfig.Height,
                MinesMap = minefieldConfig.MinesMap,
                OpenMap = new HashSet<int>(),
                MarkMap = new HashSet<int>()
            });

            m_CoordinatesHandler = new CoordinatesHandler(minefieldConfig.Width, minefieldConfig.Height);
            m_AroundIndexCalculator = new AroundIndexCalculator(m_CoordinatesHandler, m_CoordinatesHandler);
        }

        public MinefieldModel(MinefieldStoredData minefieldStoredData)
        {
            RestoreData(minefieldStoredData);
        }

        public MinefieldStoredData StoreData()
        {
            return new MinefieldStoredData()
            {
                Width = m_Data.Width,
                Height = m_Data.Height,
                MinesMap = m_Data.MinesMap,
                OpenMap = m_Data.OpenMap,
                MarkMap = m_Data.MarkMap,
            };
        }

        public void RestoreData(MinefieldStoredData minefieldStoredData)
        {
            m_Data = new MinefieldData(new MinefieldDataConfig()
            {
                Width = minefieldStoredData.Width,
                Height = minefieldStoredData.Height,
                MinesMap = minefieldStoredData.MinesMap != null ? minefieldStoredData.MinesMap : new HashSet<int>(),
                OpenMap = minefieldStoredData.OpenMap != null ? minefieldStoredData.OpenMap : new HashSet<int>(),
                MarkMap = minefieldStoredData.MarkMap != null ? minefieldStoredData.MarkMap : new HashSet<int>()
            });

            m_CoordinatesHandler = new CoordinatesHandler(minefieldStoredData.Width, minefieldStoredData.Height);
            m_AroundIndexCalculator = new AroundIndexCalculator(m_CoordinatesHandler, m_CoordinatesHandler);
        }

        public CellCondition GetCellCondition(int index)
        {
            return new CellCondition()
            {
                IsOpen = m_Data.OpenMap.Contains(index),
                IsMark = m_Data.MarkMap.Contains(index),
            };
        }

        public CellDefinition GetCellDefinition(int index)
        {
            return new CellDefinition()
            {
                IsMine = m_Data.MinesMap.Contains(index),
                NumMinesAround = GetNumMinesAroundCell(index),
            };
        }

        public CellState GetCellState(int index)
        {
            return m_CellConditionToStateConverter.GetCellState(GetCellDefinition(index), GetCellCondition(index));
        }

        public MinefieldState GetMinefieldState()
        {
            var minefieldState = new MinefieldState()
            {
                State = new CellState[m_Data.Width, m_Data.Height]
            };

            for (var i = 0; i < m_Data.Height; i++)
            {
                for (var j = 0; j < m_Data.Width; j++)
                {
                    var index = i * m_Data.Width + j;
                    minefieldState.State[j, i] = GetCellState(index);
                }
            }

            return minefieldState;
        }

        public bool GetIsOpen(int index)
        {
            return m_Data.OpenMap.Contains(index);
        }

        public bool GetIsMark(int index)
        {
            return m_Data.MarkMap.Contains(index);
        }

        public bool GetIsMine(int index)
        {
            return m_Data.MinesMap.Contains(index);
        }

        public int GetNumMinesAround(int index)
        {
            return GetNumMinesAroundCell(index);
        }

        public void SetIsOpen(int index, bool isOpen)
        {
            if (isOpen)
            {
                m_Data.OpenMap.Add(index);
            }
            else
            {
                m_Data.OpenMap.Remove(index);
            }
        }

        public void SetIsMark(int index, bool isMark)
        {
            if (isMark)
            {
                m_Data.MarkMap.Add(index);
            }
            else
            {
                m_Data.MarkMap.Remove(index);
            }
        }

        private int GetNumMinesAroundCell(int index)
        {
            var aroundIndexes = m_AroundIndexCalculator.GetAllAroundIndexes(index);

            var numMines = 0;
            foreach (var i in aroundIndexes)
            {
                if (m_Data.MinesMap.Contains(i))
                {
                    numMines++;
                }
            }

            return numMines;
        }
    }
}