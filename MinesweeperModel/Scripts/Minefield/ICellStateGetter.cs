namespace Minesweeper.Minefield
{
    internal interface ICellStateGetter
    {
        CellState GetCellState(int index);
    }
}