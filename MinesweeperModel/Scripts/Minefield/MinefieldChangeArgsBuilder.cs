using Minesweeper.Minefield.Coordinates;
using Minesweeper.MinesweeperStates;

namespace Minesweeper.Minefield
{
    internal class MinefieldChangeArgsBuilder
    {
        private readonly ICoordinatesConverter m_Converter;
        private readonly ICellStateGetter m_CellStateGetter;
        private readonly IMatchStateHolder m_MatchStateHolder;
        private readonly HashSet<int> m_ChangedIndexes = new HashSet<int>();

        public MinefieldChangeArgsBuilder(
            ICoordinatesConverter converter,
            ICellStateGetter cellStateGetter,
            IMatchStateHolder matchStateHolder)
        {
            m_CellStateGetter = cellStateGetter;
            m_MatchStateHolder = matchStateHolder;
            m_Converter = converter;
        }

        public void AddChange(int index)
        {
            m_ChangedIndexes.Add(index);
        }

        public MinefieldChangeArgs Build()
        {
            var minefieldChangeArgs = new MinefieldChangeArgs();
            var changeArgs = new List<CellChangeArgs>();
            minefieldChangeArgs.CellsChangeArgs = changeArgs;
            foreach (var changedIndex in m_ChangedIndexes)
            {
                var coords = m_Converter.ConvertIndexToCoordinates(changedIndex);
                changeArgs.Add(new CellChangeArgs()
                {
                    Coordinates = coords,
                    CellState = m_CellStateGetter.GetCellState(changedIndex)
                });
            }

            minefieldChangeArgs.MatchState = m_MatchStateHolder.CurrentMatchState;
            return minefieldChangeArgs;
        }
    }
}