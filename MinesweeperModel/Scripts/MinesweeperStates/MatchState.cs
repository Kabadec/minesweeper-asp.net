namespace Minesweeper.MinesweeperStates
{
    public enum MatchState
    {
        None = -1,
        Start = 0,
        InGame = 1,
        Win = 2,
        Lose = 3
    }
}