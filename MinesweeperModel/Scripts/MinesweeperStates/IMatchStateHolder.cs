namespace Minesweeper.MinesweeperStates;

public interface IMatchStateHolder
{
    MatchState CurrentMatchState { get; }
}