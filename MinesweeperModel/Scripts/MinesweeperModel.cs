﻿using Minesweeper.Minefield;
using Minesweeper.Minefield.Coordinates;
using Minesweeper.MinesweeperStates;

namespace Minesweeper
{
    public class MinesweeperModel : IMinesweeperModel, IMatchStateHolder
    {
        private readonly ILogger m_Logger;
        private readonly ITime m_Time;
        private readonly bool m_IsTestMode;

        private MinefieldModel m_MinefieldModel;
        private MatchState m_CurrentMatchState;
        private DateTime m_TimeStartGame;
        private DateTime m_TimeEndGame;
        private int m_OpenedMineIndex;

        public int Width => m_MinefieldModel.Width;
        public int Height => m_MinefieldModel.Height;
        public int NumOpenedCells => m_MinefieldModel.NumOpenedCells;
        public int NumMines => m_MinefieldModel.NumMines;
        public MatchState CurrentMatchState => m_CurrentMatchState;
        public bool IsGameEnd => GetIsGameEnd();
        public DateTime TimeStartGame => m_TimeStartGame;
        public DateTime TimeEndGame => m_TimeEndGame;
        public int NumMarks => m_MinefieldModel.NumMarks;

        public MinesweeperModel(MinesweeperModelConfig config)
        {
            m_IsTestMode = config.IsTestMode;
            m_Logger = config.Logger;
            m_Time = config.Time;
            m_CurrentMatchState = MatchState.None;
            m_MinefieldModel = new MinefieldModel();
        }

        public MinesweeperModel(MinesweeperModelConfig config, MinefieldConfig minefieldConfig) : this(config)
        {
            Configure(minefieldConfig);
        }

        public MinesweeperModel(MinesweeperModelConfig config, MinesweeperStoredData minesweeperStoredData) :
            this(config)
        {
            RestoreData(minesweeperStoredData);
        }

        public void Configure(MinefieldConfig minefieldConfig)
        {
            m_MinefieldModel.Configure(minefieldConfig);
            m_CurrentMatchState = MatchState.Start;
            m_TimeStartGame = DateTime.MinValue;
            m_TimeEndGame = DateTime.MinValue;
        }

        public void RestoreData(MinesweeperStoredData minesweeperStoredData)
        {
            m_CurrentMatchState = minesweeperStoredData.CurrentMatchState;
            m_TimeStartGame = minesweeperStoredData.TimeStartGame;
            m_TimeEndGame = minesweeperStoredData.TimeEndGame;
            m_MinefieldModel = new MinefieldModel(minesweeperStoredData.MinefieldStoredData);
        }

        public MinesweeperStoredData StoreData()
        {
            return new MinesweeperStoredData()
            {
                CurrentMatchState = m_CurrentMatchState,
                TimeStartGame = m_TimeStartGame,
                TimeEndGame = m_TimeEndGame,
                MinefieldStoredData = m_MinefieldModel.StoreData()
            };
        }

        public MinefieldState GetMinefieldState()
        {
            return m_MinefieldModel.GetMinefieldState();
        }

        public CellState GetCellState(CellCoordinates coords)
        {
            if (!ValidateCoordinates(coords))
            {
                return CellState.IdleClosed;
            }

            var index = m_MinefieldModel.CoordinatesConverter.ConvertCoordinatesToIndex(coords);
            return m_MinefieldModel.GetCellState(index);
        }

        public bool IsCellOpened(CellCoordinates coords)
        {
            if (!GetIsGameExist() || !ValidateCoordinates(coords))
            {
                return false;
            }

            var index = m_MinefieldModel.CoordinatesConverter.ConvertCoordinatesToIndex(coords);
            return m_MinefieldModel.GetIsOpen(index);
        }

        public MinefieldChangeArgs TryOpenCell(CellCoordinates coords)
        {
            var changeArgsBuilder = CreateChangeArgsBuilder();
            if (!GetIsGameExist() || GetIsGameEnd() || !ValidateCoordinates(coords))
            {
                return changeArgsBuilder.Build();
            }

            var index = m_MinefieldModel.CoordinatesConverter.ConvertCoordinatesToIndex(coords);
            OpenCell(index, changeArgsBuilder);
            return changeArgsBuilder.Build();
        }

        public MinefieldChangeArgs TryOpenBlock(CellCoordinates blockCenter)
        {
            var changeArgsBuilder = CreateChangeArgsBuilder();
            if (!GetIsGameExist() || GetIsGameEnd() || !ValidateCoordinates(blockCenter))
            {
                return changeArgsBuilder.Build();
            }

            var blockIndex = m_MinefieldModel.CoordinatesConverter.ConvertCoordinatesToIndex(blockCenter);
            if (!m_MinefieldModel.GetIsOpen(blockIndex) ||
                m_MinefieldModel.GetIsMark(blockIndex))
            {
                return changeArgsBuilder.Build();
            }

            var index = m_MinefieldModel.CoordinatesConverter.ConvertCoordinatesToIndex(blockCenter);
            var aroundIndexes = m_MinefieldModel.AroundIndexCalculator.GetAllAroundIndexes(index);
            var numMines = m_MinefieldModel.GetCellDefinition(blockIndex).NumMinesAround;
            var numMarks = 0;

            foreach (var aroundIndex in aroundIndexes)
            {
                numMarks += HasMark(aroundIndex) ? 1 : 0;
            }

            if (numMarks != numMines)
            {
                return changeArgsBuilder.Build();
            }

            var cellsForOpen = new List<int>();
            foreach (var aroundIndex in aroundIndexes)
            {
                if (NeedOpen(aroundIndex))
                {
                    cellsForOpen.Add(aroundIndex);
                }
            }

            foreach (var cellForOpenIndex in cellsForOpen)
            {
                OpenCell(cellForOpenIndex, changeArgsBuilder);
            }

            return changeArgsBuilder.Build();

            bool HasMark(int targetCellIndex)
            {
                return m_MinefieldModel.CoordinatesValidator.ValidateIndex(targetCellIndex) &&
                       !m_MinefieldModel.GetIsOpen(targetCellIndex) &&
                       m_MinefieldModel.GetIsMark(targetCellIndex);
            }

            bool NeedOpen(int targetCellIndex)
            {
                return m_MinefieldModel.CoordinatesValidator.ValidateIndex(targetCellIndex) &&
                       !m_MinefieldModel.GetIsOpen(targetCellIndex) &&
                       !m_MinefieldModel.GetIsMark(targetCellIndex);
            }
        }

        public MinefieldChangeArgs TrySwitchMark(CellCoordinates coords)
        {
            var changeArgsBuilder = CreateChangeArgsBuilder();
            if (!GetIsGameExist() || GetIsGameEnd() || !ValidateCoordinates(coords))
            {
                return changeArgsBuilder.Build();
            }

            var index = m_MinefieldModel.CoordinatesConverter.ConvertCoordinatesToIndex(coords);
            if (m_MinefieldModel.GetIsOpen(index))
            {
                return changeArgsBuilder.Build();
            }

            var isMarked = m_MinefieldModel.GetIsMark(index);
            m_MinefieldModel.SetIsMark(index, !isMarked);
            changeArgsBuilder.AddChange(index);
            return changeArgsBuilder.Build();
        }

        private bool OpenCell(int index, MinefieldChangeArgsBuilder changeArgsBuilder)
        {
            if (!m_MinefieldModel.CoordinatesValidator.ValidateIndex(index) ||
                m_MinefieldModel.GetIsOpen(index) ||
                m_MinefieldModel.GetIsMark(index))
            {
                return false;
            }

            m_MinefieldModel.SetIsOpen(index, true);
            changeArgsBuilder.AddChange(index);

            if (m_MinefieldModel.GetIsMine(index))
            {
                LoseGame(index);
                return true;
            }

            if (NumOpenedCells == (m_MinefieldModel.NumCells - m_MinefieldModel.NumMines))
            {
                WinGame();
                return true;
            }

            if (m_CurrentMatchState == MatchState.Start)
            {
                StartGame();
            }

            if (m_MinefieldModel.GetNumMinesAround(index) > 0)
            {
                return true;
            }

            var aroundIndexes = m_MinefieldModel.AroundIndexCalculator.GetAllAroundIndexes(index);
            foreach (var aroundIndex in aroundIndexes)
            {
                OpenCell(aroundIndex, changeArgsBuilder);
            }

            return true;
        }

        private bool ValidateCoordinates(CellCoordinates coords)
        {
            if (!m_MinefieldModel.CoordinatesValidator.ValidateCoordinates(coords))
            {
                m_Logger.Log(LogType.Error, "Coordinates not valid!");
                return false;
            }

            return true;
        }

        private MinefieldChangeArgsBuilder CreateChangeArgsBuilder()
        {
            return new MinefieldChangeArgsBuilder(m_MinefieldModel.CoordinatesConverter, m_MinefieldModel, this);
        }

        private bool GetIsGameExist()
        {
            return m_CurrentMatchState != MatchState.None;
        }

        private bool GetIsGameEnd()
        {
            if (m_IsTestMode)
            {
                return false;
            }

            return m_CurrentMatchState == MatchState.Win || m_CurrentMatchState == MatchState.Lose;
        }

        private void StartGame()
        {
            if (GetIsGameEnd())
            {
                return;
            }

            m_CurrentMatchState = MatchState.InGame;
            m_TimeStartGame = m_Time.Now;
        }

        private void WinGame()
        {
            if (GetIsGameEnd())
            {
                return;
            }

            m_CurrentMatchState = MatchState.Win;
            m_TimeEndGame = m_Time.Now;
        }

        private void LoseGame(int openedMineIndex)
        {
            if (GetIsGameEnd())
            {
                return;
            }

            m_CurrentMatchState = MatchState.Lose;
            m_OpenedMineIndex = openedMineIndex;
            m_TimeEndGame = m_Time.Now;
        }
    }
}