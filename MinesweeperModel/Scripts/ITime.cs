namespace Minesweeper;

public interface ITime
{
    DateTime Now { get; }
}