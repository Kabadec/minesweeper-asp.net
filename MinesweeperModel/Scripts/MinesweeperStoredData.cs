﻿using Minesweeper.Minefield.Data;
using Minesweeper.MinesweeperStates;

namespace Minesweeper;

public class MinesweeperStoredData
{
    public MatchState CurrentMatchState;
    public DateTime TimeStartGame;
    public DateTime TimeEndGame;
    public MinefieldStoredData MinefieldStoredData;
}