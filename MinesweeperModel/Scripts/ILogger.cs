namespace Minesweeper
{
    public interface ILogger
    {
        void Log(LogType type, string message);
    }

    public enum LogType
    {
        Error,
        Warning,
        Log
    }
}