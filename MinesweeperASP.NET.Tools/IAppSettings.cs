namespace Tools;

public interface IAppSettings
{
    string DbConnectionString { get; }
}