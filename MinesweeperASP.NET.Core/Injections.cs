﻿using Db;
using Microsoft.Extensions.DependencyInjection;
using Services;
using Tools;

namespace Core;

public class Injections
{
    public static void AddServices(IServiceCollection services, IAppSettings settings)
    {
        services.AddDb(settings)
                .AddServices(settings);
    }
}