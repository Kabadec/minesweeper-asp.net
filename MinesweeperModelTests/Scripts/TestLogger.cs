using System;
using Minesweeper;

namespace MinesweeperTests
{
    public class TestLogger : ILogger
    {
        public void Log(LogType type, string message)
        {
            Console.WriteLine($"{type.ToString()}: {message}");
        }
    }
}