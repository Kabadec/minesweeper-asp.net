using Minesweeper;
using Minesweeper.Minefield;

namespace MinesweeperTests
{
    public static class TestMinesweeperCreator
    {
        public static IMinesweeperModel CreateModel()
        {
            var minefieldConfig = CreateMinesField();
            var config = new MinesweeperModelConfig()
            {
                Logger = new TestLogger(),
                Time = new TestTime()
            };

            return new MinesweeperModel(config, minefieldConfig);
        }

        public static MinefieldConfig CreateMinesField()
        {
            bool[,] minesField =
            {
                { false, false, false, false,  true, false, false, false, false,  true },
                { false, false, false, false,  true, false, false, false, false, false },
                { false, false, false, false,  true, false,  true,  true,  true, false },
                { false, false,  true,  true, false, false,  true, false,  true, false },
                { false,  true, false, false, false, false,  true,  true,  true, false },
                {  true, false, false, false, false, false, false, false, false, false },
                { false, false, false, false, false, false, false, false, false, false },
                { false, false, false, false, false, false,  true,  true,  true, false },
                {  true,  true, false, false, false, false, false, false, false, false },
                {  true,  true,  true,  true,  true, false,  true, false, false, false },
            };
            
            var minesMap = new HashSet<int>();
            var width = minesField.GetLength(0);
            var height = minesField.GetLength(1);
            for (var i = 0; i < height; i++)
            {
                for (var j = 0; j < width; j++)
                {
                    if (minesField[j, i])
                    {
                        var index = i * width + j;
                        minesMap.Add(index);
                    }
                }
            }
            
            return new MinefieldConfig()
            {
                Width = width,
                Height = height,
                MinesMap = minesMap
            };
        }
    }
}