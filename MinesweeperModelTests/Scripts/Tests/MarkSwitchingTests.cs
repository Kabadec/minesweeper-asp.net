using Minesweeper;
using Minesweeper.Minefield;
using Minesweeper.Minefield.Coordinates;
using Xunit;

namespace MinesweeperTests.Tests
{
    public class MarkSwitchingTests
    {
        private readonly IMinesweeperModel m_MinesweeperModel;

        public MarkSwitchingTests()
        {
            m_MinesweeperModel = TestMinesweeperCreator.CreateModel();
        }

        [Fact]
        public void Test_MarkSwitch()
        {
            var minefieldState = m_MinesweeperModel.GetMinefieldState();
            for (var i = 0; i < minefieldState.State.GetLength(0); i++)
            {
                for (var j = 0; j < minefieldState.State.GetLength(1); j++)
                {
                    var cellState = minefieldState.State[i, j];
                    Assert.NotEqual(CellState.Marked, cellState);
                }
            }

            var highlightedCellCoordinates = new CellCoordinates(5, 5);
            m_MinesweeperModel.TrySwitchMark(highlightedCellCoordinates);
            Assert.Equal(CellState.Marked, m_MinesweeperModel.GetCellState(highlightedCellCoordinates));
            minefieldState = m_MinesweeperModel.GetMinefieldState();
            for (var i = 0; i < minefieldState.State.GetLength(0); i++)
            {
                for (var j = 0; j < minefieldState.State.GetLength(1); j++)
                {
                    if (i == 5 && j == 5)
                    {
                        continue;
                    }

                    var cellState = minefieldState.State[i, j];
                    Assert.NotEqual(CellState.Marked, cellState);
                }
            }
        }

        [Fact]
        public void Test_TryMarkOpenedCell()
        {
            var openedCellCoordinatesFirst = new CellCoordinates(5, 5);
            m_MinesweeperModel.TryOpenCell(openedCellCoordinatesFirst);
            m_MinesweeperModel.TrySwitchMark(openedCellCoordinatesFirst);
            Assert.NotEqual(CellState.Marked, m_MinesweeperModel.GetCellState(openedCellCoordinatesFirst));
        }

        [Fact]
        public void Test_TrySwitchMarkedCell()
        {
            var markedCellCoordinatesFirst = new CellCoordinates(5, 5);
            m_MinesweeperModel.TrySwitchMark(markedCellCoordinatesFirst);
            m_MinesweeperModel.TrySwitchMark(markedCellCoordinatesFirst);
            Assert.NotEqual(CellState.Marked, m_MinesweeperModel.GetCellState(markedCellCoordinatesFirst));
        }
        
        [Fact]
        public void Test_MarkSeveralCell()
        {
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(5, 5));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(6, 5));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(1, 3));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(5, 8));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(7, 0));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(4, 3));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(8, 7));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(5, 5));
            Assert.Equal(6, m_MinesweeperModel.NumMarks);
        }
    }
}