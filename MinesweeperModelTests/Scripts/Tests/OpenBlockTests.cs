using Minesweeper;
using Minesweeper.Minefield;
using Minesweeper.Minefield.Coordinates;
using Xunit;

namespace MinesweeperTests.Tests
{
    public class OpenBlockTests
    {
        private readonly IMinesweeperModel m_MinesweeperModel;

        public OpenBlockTests()
        {
            m_MinesweeperModel = TestMinesweeperCreator.CreateModel();
        }

        [Fact]
        public void Test_UnopenedCenter()
        {
            m_MinesweeperModel.TryOpenBlock(new CellCoordinates(0, 0));
            Assert.Equal(CellState.IdleClosed, m_MinesweeperModel.GetCellState(new CellCoordinates(0, 0)));
        }

        [Fact]
        public void Test_MarkedCenter()
        {
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(0, 0));
            m_MinesweeperModel.TryOpenBlock(new CellCoordinates(0, 0));
            Assert.Equal(CellState.Marked, m_MinesweeperModel.GetCellState(new CellCoordinates(0, 0)));
        }

        [Fact]
        public void Test_NotEnoughMarkedCells()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TryOpenBlock(new CellCoordinates(2, 1));
            Assert.Equal(CellState.IdleClosed, m_MinesweeperModel.GetCellState(new CellCoordinates(3, 2)));
        }

        [Fact]
        public void Test_EnoughMarkedCells_First()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(3, 2));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(4, 1));
            m_MinesweeperModel.TryOpenBlock(new CellCoordinates(3, 1));
            Assert.Equal(CellState.DigitTwo, m_MinesweeperModel.GetCellState(new CellCoordinates(4, 0)));
            Assert.Equal(CellState.DigitThree, m_MinesweeperModel.GetCellState(new CellCoordinates(4, 2)));
        }
        
        [Fact]
        public void Test_EnoughMarkedCells_Second()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(3, 2));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(3, 3));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(1, 4));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(2, 4));
            m_MinesweeperModel.TryOpenBlock(new CellCoordinates(2, 3));
            Assert.Equal(CellState.DigitTwo, m_MinesweeperModel.GetCellState(new CellCoordinates(3, 4)));
        }
        
        [Fact]
        public void Test_TooMuchMarkedCells()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(3, 2));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(4, 2));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(4, 1));
            m_MinesweeperModel.TryOpenBlock(new CellCoordinates(3, 1));
            Assert.Equal(CellState.IdleClosed, m_MinesweeperModel.GetCellState(new CellCoordinates(4, 0)));
        }
    }
}