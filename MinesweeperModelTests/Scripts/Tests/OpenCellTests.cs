using Minesweeper;
using Minesweeper.Minefield;
using Minesweeper.Minefield.Coordinates;
using Xunit;

namespace MinesweeperTests.Tests
{
    public class OpenCellTests
    {
        private readonly IMinesweeperModel m_MinesweeperModel;

        public OpenCellTests()
        {
            m_MinesweeperModel = TestMinesweeperCreator.CreateModel();
        }

        [Fact]
        public void Test_OpenEmptyCell()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            for (var i = 4; i <= 9; i++)
            {
                for (var j = 0; j <= 9; j++)
                {
                    Assert.NotEqual(CellState.IdleOpened, m_MinesweeperModel.GetCellState(new CellCoordinates(i, j)));
                }
            }

            for (var i = 0; i <= 3; i++)
            {
                for (var j = 4; j <= 9; j++)
                {
                    Assert.NotEqual(CellState.IdleOpened, m_MinesweeperModel.GetCellState(new CellCoordinates(i, j)));
                }
            }

            Assert.NotEqual(CellState.IdleOpened, m_MinesweeperModel.GetCellState(new CellCoordinates(4, 0)));
            Assert.NotEqual(CellState.IdleOpened, m_MinesweeperModel.GetCellState(new CellCoordinates(3, 2)));
            Assert.NotEqual(CellState.IdleOpened, m_MinesweeperModel.GetCellState(new CellCoordinates(3, 3)));
        }

        [Fact]
        public void Test_OpenEmptyCellWithWrongMark()
        {
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(1, 1));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            Assert.NotEqual(CellState.IdleOpened, m_MinesweeperModel.GetCellState(new CellCoordinates(1, 1)));
        }

        [Fact]
        public void Test_DemarkWrongCellAndOpen()
        {
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(1, 1));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(1, 1));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(1, 1));
            Assert.NotEqual(CellState.IdleClosed, m_MinesweeperModel.GetCellState(new CellCoordinates(1, 1)));
        }


        [Fact]
        public void Test_OpenCellNearMine()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(6, 6)); ;
            Assert.Equal(CellState.DigitTwo, m_MinesweeperModel.GetCellState(new CellCoordinates(6, 6)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(5, 5)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(5, 6)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(5, 7)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(6, 5)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(6, 7)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(7, 5)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(7, 6)));
            Assert.Equal(CellState.IdleClosed,m_MinesweeperModel.GetCellState(new CellCoordinates(7, 7)));
        }
        
        [Fact]
        public void Test_OpenMine()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(4, 6));
            Assert.NotEqual(CellState.IdleClosed, m_MinesweeperModel.GetCellState(new CellCoordinates(4, 6)));
        }
        
        [Fact]
        public void Test_TryOpenOpenedCell()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            Assert.NotEqual(CellState.IdleClosed, m_MinesweeperModel.GetCellState(new CellCoordinates(0, 0)));
        }

        [Fact]
        public void Test_TryOpenMarkedCell()
        {
            m_MinesweeperModel.TrySwitchMark(new CellCoordinates(0, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            Assert.Equal(CellState.Marked, m_MinesweeperModel.GetCellState(new CellCoordinates(0, 0)));
        }
    }
}