using Minesweeper;
using Minesweeper.Minefield;
using Xunit;

namespace MinesweeperTests.Tests
{
    public class IdleTests
    {
        private readonly IMinesweeperModel m_MinesweeperModel;

        public IdleTests()
        {
            m_MinesweeperModel = TestMinesweeperCreator.CreateModel();
        }

        [Fact]
        public void Test_Idle()
        {
            var minefieldCondition = m_MinesweeperModel.GetMinefieldState();
            for (var i = 0; i < minefieldCondition.State.GetLength(0); i++)
            {
                for (var j = 0; j < minefieldCondition.State.GetLength(1); j++)
                {
                    var cellState = minefieldCondition.State[i, j];
                    Assert.Equal(CellState.IdleClosed, cellState);
                }
            }
        }
    }
}