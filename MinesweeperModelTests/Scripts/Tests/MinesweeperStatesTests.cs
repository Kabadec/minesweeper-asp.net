using Minesweeper;
using Minesweeper.Minefield.Coordinates;
using Minesweeper.MinesweeperStates;
using Xunit;

namespace MinesweeperTests.Tests
{
    public class MinesweeperStatesTests
    {
        private readonly IMinesweeperModel m_MinesweeperModel;

        public MinesweeperStatesTests()
        {
            m_MinesweeperModel = TestMinesweeperCreator.CreateModel();
        }


        [Fact]
        public void Test_StartGame()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(9, 9));
            Assert.Equal(m_MinesweeperModel.CurrentMatchState, MatchState.InGame);
        }

        [Fact]
        public void Test_LoseGame()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 4));
            Assert.Equal(m_MinesweeperModel.CurrentMatchState, MatchState.Lose);
        }

        [Fact]
        public void Test_InGameToLoseGame()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            Assert.Equal(m_MinesweeperModel.CurrentMatchState, MatchState.InGame);
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 4));
            Assert.Equal(m_MinesweeperModel.CurrentMatchState, MatchState.Lose);
        }

        [Fact]
        public void Test_WinGame()
        {
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(0, 6));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(5, 3));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(9, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(1, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(2, 5));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(2, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(3, 4));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(3, 5));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(3, 7));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(3, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(4, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(4, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(5, 6));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(5, 7));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(5, 8));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(5, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(6, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(6, 6));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(6, 7));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(6, 8));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(6, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(7, 0));
            m_MinesweeperModel.TryOpenCell(new CellCoordinates(7, 9));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(8, 6));

            m_MinesweeperModel.TryOpenCell(new CellCoordinates(9, 5));
            Assert.Equal(m_MinesweeperModel.CurrentMatchState, MatchState.Win);
        }
    }
}