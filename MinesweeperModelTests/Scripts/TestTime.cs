using Minesweeper;

namespace MinesweeperTests;

public class TestTime : ITime
{
    public DateTime Now => DateTime.Now;
}