namespace Services.Services;

public class TimeService : ITimeService
{
    private readonly DateTime m_DateTimeStart = new DateTime(1970, 1, 1);
    public DateTime UtcNow => DateTime.UtcNow;
    public double UtsNowUnits => DateTimeToUnits(UtcNow);

    public double DateTimeToUnits(DateTime dateTime)
    {
        return new TimeSpan(dateTime.Ticks - m_DateTimeStart.Ticks).TotalMilliseconds;
    }

    public DateTime UnitsToDateTime(double units)
    {
        return m_DateTimeStart + TimeSpan.FromMilliseconds(units);
    }
}