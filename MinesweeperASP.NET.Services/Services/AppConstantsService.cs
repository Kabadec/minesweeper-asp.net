namespace Services.Services;

public class AppConstantsService : IAppConstantsService
{
    public bool IsTestMode => true;
    public TimeSpan MemoryCacheLifeTime => TimeSpan.FromMinutes(120);
    public string DefaultDifficult => "Beginner";
    public string MatchDataKey => "MatchData";
    public string CookieSessionIdKey => "SessionId";
    public string IsMatchEnteredIntoDataBaseKey => "IsMatchEnteredIntoDataBase";
}