namespace Services.Services;

public interface ITimeService
{
    DateTime UtcNow { get; }
    double UtsNowUnits { get; }
    double DateTimeToUnits(DateTime dateTime);
    DateTime UnitsToDateTime(double units);
}