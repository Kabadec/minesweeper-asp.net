namespace Services.Services;

public interface IAppConstantsService
{
    bool IsTestMode { get; }
    TimeSpan MemoryCacheLifeTime { get; }
    string DefaultDifficult { get; }
    string MatchDataKey { get; }
    string CookieSessionIdKey { get; }
    string IsMatchEnteredIntoDataBaseKey { get; }
}