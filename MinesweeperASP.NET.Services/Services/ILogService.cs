namespace Services.Services;

public interface ILogService
{
    void Log(string log);
}