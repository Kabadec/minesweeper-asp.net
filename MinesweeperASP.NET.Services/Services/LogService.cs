namespace Services.Services;

public class LogService : ILogService
{
    public void Log(string log)
    {
        Console.WriteLine("--- START LOG ---");
        Console.WriteLine(log);
        Console.WriteLine("--- END LOG ---");
    }
}