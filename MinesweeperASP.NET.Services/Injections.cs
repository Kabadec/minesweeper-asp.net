using Microsoft.Extensions.DependencyInjection;
using Services.Services;
using Tools;

namespace Services;

public static class Injections
{
    public static IServiceCollection AddServices(this IServiceCollection services, IAppSettings settings)
    {
        services.AddTransient<ITimeService, TimeService>();
        services.AddTransient<IAppConstantsService, AppConstantsService>();
        services.AddTransient<ILogService, LogService>();

        return services;
    }
}
