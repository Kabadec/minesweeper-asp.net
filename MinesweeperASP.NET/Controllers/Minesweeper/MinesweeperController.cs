﻿using Db.Models;
using System.Text.Json;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using Minesweeper;
using Minesweeper.Minefield;
using Minesweeper.Minefield.Coordinates;
using MinesweeperASPNET.MinesweeperModelBridge;
using MinesweeperASPNET.MinesweeperModelBridge.Datas;
using MinesweeperASPNET.MinesweeperModelBridge.Dtos;
using Services.Services;

namespace MinesweeperASPNET.Controllers.Minesweeper;

public class MinesweeperController : Controller
{
    private readonly ApplicationContext m_Db;
    private readonly IMemoryCache m_MemoryCache;
    private readonly ITimeService m_TimeService;
    private readonly IAppConstantsService m_AppConstantsService;
    private readonly ILogService m_LogService;
    private readonly MinefieldConfigCreator m_MinefieldConfigCreator;
    private readonly IMinesweeperModel m_MinesweeperModel;
    private readonly MinesweeperSerializer m_MinesweeperSerializer;
    private readonly MinesweeperDataConvertor m_MinesweeperDataConvertor;

    public MinesweeperController(ApplicationContext context,
        IMemoryCache memoryCache,
        ITimeService timeService,
        IAppConstantsService appConstantsService,
        ILogService logService)
    {
        m_Db = context;
        m_MemoryCache = memoryCache;
        m_TimeService = timeService;
        m_AppConstantsService = appConstantsService;
        m_LogService = logService;
        m_MinefieldConfigCreator = new MinefieldConfigCreator(m_AppConstantsService.DefaultDifficult);
        m_MinesweeperSerializer = new MinesweeperSerializer();
        m_MinesweeperDataConvertor = new MinesweeperDataConvertor(m_TimeService);
        m_MinesweeperModel = new MinesweeperModel(new MinesweeperModelConfig()
        {
            IsTestMode = false,
            Logger = new MinesweeperLogger(),
            Time = new MinesweeperTime(m_TimeService)
        });
    }

    private void TryRestoreMatchData()
    {
        if (!HttpContext.Session.TryGetValue(m_AppConstantsService.MatchDataKey, out var data))
        {
            return;
        }

        var minesweeperSerializableData = m_MinesweeperSerializer.DeserializeMinesweeperData(data);
        var minesweeperStoredData = m_MinesweeperDataConvertor.SerializableToStoredData(minesweeperSerializableData);
        m_MinesweeperModel.RestoreData(minesweeperStoredData);
    }

    public IActionResult Index()
    {
        const string filePath = "~/index.html";
        return File(filePath, "text/html");
    }

    public IActionResult FieldState()
    {
        TryRestoreMatchData();
        var minesweeperStateDto = CrateMinesweeperStateDto();
        var jsonResult = new JsonResult(minesweeperStateDto);
        m_LogService.Log(JsonSerializer.Serialize(jsonResult.Value));
        return jsonResult;
    }

    public IActionResult StartNewMatch(string difficult)
    {
        var minefieldConfig = m_MinefieldConfigCreator.CreateMinefieldConfig(difficult);
        m_MinesweeperModel.Configure(minefieldConfig);
        UpdateMatchData();
        HttpContext.Session.Set<bool>(m_AppConstantsService.IsMatchEnteredIntoDataBaseKey, false);
        var minesweeperStateDto = CrateMinesweeperStateDto();
        var jsonResult = new JsonResult(minesweeperStateDto);
        m_LogService.Log(JsonSerializer.Serialize(jsonResult.Value));
        return jsonResult;
    }

    public IActionResult OpenCell(int x = -1, int y = -1)
    {
        TryRestoreMatchData();
        var coords = new CellCoordinates(x, y);
        var isOpen = m_MinesweeperModel.IsCellOpened(coords);
        var minefieldChangeArgs =
            isOpen ? m_MinesweeperModel.TryOpenBlock(coords) : m_MinesweeperModel.TryOpenCell(coords);
        UpdateMatchData();
        var minesweeperChangeArgsDto = CreateMinesweeperChangeArgsDto(minefieldChangeArgs);
        var jsonResult = new JsonResult(minesweeperChangeArgsDto);
        var jsonString = JsonSerializer.Serialize(jsonResult.Value);
        m_LogService.Log(jsonString);
        return jsonResult;
    }

    public IActionResult MarkCell(int x = -1, int y = -1)
    {
        TryRestoreMatchData();
        var minefieldChangeArgs = m_MinesweeperModel.TrySwitchMark(new CellCoordinates(x, y));
        UpdateMatchData();
        var minesweeperChangeArgsDto = CreateMinesweeperChangeArgsDto(minefieldChangeArgs);
        var jsonResult = new JsonResult(minesweeperChangeArgsDto);
        var jsonString = JsonSerializer.Serialize(jsonResult.Value);
        m_LogService.Log(jsonString);
        return jsonResult;
    }

    private void UpdateMatchData()
    {
        var minesweeperStoredData = m_MinesweeperModel.StoreData();
        var minesweeperSerializableData = m_MinesweeperDataConvertor.StoredToSerializableData(minesweeperStoredData);
        var data = m_MinesweeperSerializer.SerializeMinesweeperData(minesweeperSerializableData);
        HttpContext.Session.Set(m_AppConstantsService.MatchDataKey, data);
        if (m_MinesweeperModel.IsGameEnd &&
            !HttpContext.Session.Get<bool>(m_AppConstantsService.IsMatchEnteredIntoDataBaseKey))
        {
            var match = new Match()
            {
                SessionId = HttpContext.Session.Id,
                UserId = "",
                TimeMatchStart = m_MinesweeperModel.TimeStartGame,
                TimeMatchEnd = m_MinesweeperModel.TimeEndGame,
                MinesweeperState = (int)m_MinesweeperModel.CurrentMatchState,
                Data = data
            };

            m_Db.MatchHistory.Add(match);
            m_Db.SaveChanges();
            HttpContext.Session.Set<bool>(m_AppConstantsService.IsMatchEnteredIntoDataBaseKey, true);
        }
    }

    private MinesweeperStateDto CrateMinesweeperStateDto()
    {
        var minefieldState = m_MinesweeperModel.GetMinefieldState();
        var minesweeperStateDto =
            new MinesweeperStateDto(
                minefieldState,
                m_MinesweeperModel.NumMines,
                m_MinesweeperModel.NumMarks,
                m_MinesweeperModel.CurrentMatchState,
                m_TimeService.DateTimeToUnits(m_MinesweeperModel.TimeStartGame),
                m_TimeService.DateTimeToUnits(m_MinesweeperModel.TimeEndGame));
        return minesweeperStateDto;
    }

    private MinesweeperChangeArgsDto CreateMinesweeperChangeArgsDto(MinefieldChangeArgs minefieldChangeArgs)
    {
        return new MinesweeperChangeArgsDto(
            minefieldChangeArgs,
            m_MinesweeperModel.NumMarks,
            m_TimeService.DateTimeToUnits(m_MinesweeperModel.TimeStartGame),
            m_TimeService.DateTimeToUnits(m_MinesweeperModel.TimeEndGame));
    }
}