﻿using System.Text.Json.Serialization;
using Minesweeper.Minefield;
using Minesweeper.MinesweeperStates;

namespace MinesweeperASPNET.MinesweeperModelBridge.Dtos;

public class MinesweeperChangeArgsDto
{
    [JsonInclude] private readonly MatchState MatchState;
    [JsonInclude] private readonly int NumMarks;
    [JsonInclude] private readonly double TimeMatchStart;
    [JsonInclude] private readonly double TimeMatchEnd;
    [JsonInclude] private readonly IReadOnlyList<CellChangeArgsDto> CellsChangeArgs;

    public MinesweeperChangeArgsDto(
        MinefieldChangeArgs minefieldChangeArgs,
        int numMarks,
        double timeMatchStart,
        double timeMatchEnd)
    {
        NumMarks = numMarks;
        TimeMatchStart = timeMatchStart;
        TimeMatchEnd = timeMatchEnd;
        MatchState = minefieldChangeArgs.MatchState;
        var cellsChangeArgs = new List<CellChangeArgsDto>();
        foreach (var cellsChangeArg in minefieldChangeArgs.CellsChangeArgs)
        {
            cellsChangeArgs.Add(new CellChangeArgsDto()
            {
                x = cellsChangeArg.Coordinates.x,
                y = cellsChangeArg.Coordinates.y,
                State = cellsChangeArg.CellState
            });
        }

        CellsChangeArgs = cellsChangeArgs;
    }
}