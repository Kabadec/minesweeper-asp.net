﻿using System.Text.Json.Serialization;
using Minesweeper.Minefield;

namespace MinesweeperASPNET.MinesweeperModelBridge.Dtos;

public struct CellChangeArgsDto
{
    [JsonInclude] public int x;
    [JsonInclude] public int y;
    [JsonInclude] public CellState State;
}