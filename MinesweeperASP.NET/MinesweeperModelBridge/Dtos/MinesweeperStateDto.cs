﻿using System.Text.Json.Serialization;
using Minesweeper.Minefield;
using Minesweeper.MinesweeperStates;

namespace MinesweeperASPNET.MinesweeperModelBridge.Dtos;

public class MinesweeperStateDto
{
    [JsonInclude] private readonly int Width;
    [JsonInclude] private readonly int Height;
    [JsonInclude] private readonly int NumMines;
    [JsonInclude] private readonly int NumMarks;
    [JsonInclude] private readonly double TimeMatchStart;
    [JsonInclude] private readonly double TimeMatchEnd;
    [JsonInclude] private readonly MatchState MatchState;
    [JsonInclude] private readonly CellState[] State;

    public MinesweeperStateDto(
        MinefieldState minefieldState,
        int numMines,
        int numMarks,
        MatchState matchState,
        double timeMatchStart,
        double timeMatchEnd)
    {
        NumMarks = numMarks;
        MatchState = matchState;
        NumMines = numMines;
        TimeMatchStart = timeMatchStart;
        TimeMatchEnd = timeMatchEnd;
        Width = minefieldState.State.GetLength(0);
        Height = minefieldState.State.GetLength(1);
        var numCells = Width * Height;
        State = new CellState[numCells];
        for (var i = 0; i < Height; i++)
        {
            for (var j = 0; j < Width; j++)
            {
                State[i * Width + j] = minefieldState.State[j, i];
            }
        }
    }
}