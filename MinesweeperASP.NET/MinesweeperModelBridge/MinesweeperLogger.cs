﻿using Minesweeper;

namespace MinesweeperASPNET.MinesweeperModelBridge;

public class MinesweeperLogger : Minesweeper.ILogger
{
    public void Log(LogType type, string message)
    {
        Console.WriteLine($"{type.ToString()}: {message}");
    }
}