using Minesweeper;
using Minesweeper.Minefield.Data;
using Services.Services;

namespace MinesweeperASPNET.MinesweeperModelBridge.Datas;

public class MinesweeperDataConvertor
{
    private readonly ITimeService m_TimeService;

    public MinesweeperDataConvertor(ITimeService timeService)
    {
        m_TimeService = timeService;
    }

    public MinesweeperStoredData SerializableToStoredData(MinesweeperSerializableData serializableData)
    {
        return new MinesweeperStoredData()
        {
            CurrentMatchState = serializableData.CurrentMatchState,
            TimeStartGame = m_TimeService.UnitsToDateTime(serializableData.TimeMatchStart),
            TimeEndGame = m_TimeService.UnitsToDateTime(serializableData.TimeMatchEnd),
            MinefieldStoredData = new MinefieldStoredData()
            {
                Width = serializableData.MinefieldSerializableData.Width,
                Height = serializableData.MinefieldSerializableData.Height,
                MinesMap = serializableData.MinefieldSerializableData.MinesMap,
                OpenMap = serializableData.MinefieldSerializableData.OpenMap,
                MarkMap = serializableData.MinefieldSerializableData.MarkMap
            }
        };
    }

    public MinesweeperSerializableData StoredToSerializableData(MinesweeperStoredData storedData)
    {
        return new MinesweeperSerializableData()
        {
            CurrentMatchState = storedData.CurrentMatchState,
            TimeMatchStart = m_TimeService.DateTimeToUnits(storedData.TimeStartGame),
            TimeMatchEnd = m_TimeService.DateTimeToUnits(storedData.TimeEndGame),
            MinefieldSerializableData = new MinefieldSerializableData()
            {
                Width = storedData.MinefieldStoredData.Width,
                Height = storedData.MinefieldStoredData.Height,
                MinesMap = storedData.MinefieldStoredData.MinesMap,
                OpenMap = storedData.MinefieldStoredData.OpenMap,
                MarkMap = storedData.MinefieldStoredData.MarkMap
            }
        };
    }
}