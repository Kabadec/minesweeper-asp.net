﻿using ProtoBuf;

namespace MinesweeperASPNET.MinesweeperModelBridge.Datas;

[ProtoContract]
public class MinefieldSerializableData
{
    [ProtoMember(1)] public int Width;
    [ProtoMember(2)] public int Height;
    [ProtoMember(3)] public HashSet<int> MinesMap;
    [ProtoMember(4)] public HashSet<int> OpenMap;
    [ProtoMember(5)] public HashSet<int> MarkMap;
}