﻿using Minesweeper.MinesweeperStates;
using ProtoBuf;

namespace MinesweeperASPNET.MinesweeperModelBridge.Datas;

[ProtoContract]
public class MinesweeperSerializableData
{
    [ProtoMember(1)] public MatchState CurrentMatchState;
    [ProtoMember(2)] public double TimeMatchStart;
    [ProtoMember(3)] public double TimeMatchEnd;
    [ProtoMember(4)] public MinefieldSerializableData MinefieldSerializableData;
}