﻿using ProtoBuf;

namespace MinesweeperASPNET.MinesweeperModelBridge.Datas;

public class MinesweeperSerializer
{
    public byte[] SerializeMinesweeperData(MinesweeperSerializableData serializableData)
    {
        using var stream = new MemoryStream();
        Serializer.Serialize(stream, serializableData);
        return stream.ToArray();
    }

    public MinesweeperSerializableData DeserializeMinesweeperData(byte[]? data)
    {
        using var stream = new MemoryStream(data);
        return Serializer.Deserialize<MinesweeperSerializableData>(stream);
    }
}