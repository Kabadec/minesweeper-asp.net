﻿using Minesweeper.Minefield;
using MinesweeperASPNET.MinesweeperModelBridge.Difficults;

namespace MinesweeperASPNET.MinesweeperModelBridge;

public class MinefieldConfigCreator
{
    private readonly DifficultMapHolder m_DifficultMapHolder = new DifficultMapHolder();
    private readonly string m_DefaultDifficult;

    public MinefieldConfigCreator(string defaultDifficult)
    {
        m_DefaultDifficult = defaultDifficult;
    }

    public MinefieldConfig CreateMinefieldConfig(string difficult)
    {
        if (string.IsNullOrEmpty(difficult) || !m_DifficultMapHolder.DifficultConfigsMap.ContainsKey(difficult))
        {
            return CreateMinefieldConfig(m_DifficultMapHolder.DifficultConfigsMap[m_DefaultDifficult]);
        }

        return CreateMinefieldConfig(m_DifficultMapHolder.DifficultConfigsMap[difficult]);
    }

    public MinefieldConfig CreateMinefieldConfig(DifficultConfig difficultConfig)
    {
        return CreateMinefieldConfig(difficultConfig.Width, difficultConfig.Height, difficultConfig.NumMines);
    }

    public MinefieldConfig CreateMinefieldConfig(int width, int height, int numMines)
    {
        var minesField = new bool[width, height];
        var minesLeft = numMines;
        var random = new Random(CreateSeed());
        while (minesLeft > 0)
        {
            var x = random.Next(0, width);
            var y = random.Next(0, height);
            if (minesField[x, y] == true)
            {
                continue;
            }

            minesField[x, y] = true;
            minesLeft--;
        }

        var minesMap = new HashSet<int>();
        for (var i = 0; i < height; i++)
        {
            for (var j = 0; j < width; j++)
            {
                if (minesField[j, i])
                {
                    var index = i * width + j;
                    minesMap.Add(index);
                }
            }
        }

        return new MinefieldConfig()
        {
            Width = width,
            Height = height,
            MinesMap = minesMap
        };
    }

    public MinefieldConfig CreateTestMinefieldConfig()
    {
        bool[,] minesField =
        {
            { false, false, false, false,  true, false, false, false, false,  true },
            { false, false, false, false,  true, false, false, false, false, false },
            { false, false, false, false,  true, false,  true,  true,  true, false },
            { false, false,  true,  true, false, false,  true, false,  true, false },
            { false,  true, false, false, false, false,  true,  true,  true, false },
            {  true, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false, false, false, false, false },
            { false, false, false, false, false, false,  true,  true,  true, false },
            {  true,  true, false, false, false, false, false, false, false, false },
            {  true,  true,  true,  true,  true, false,  true, false, false, false },
        };

        var minesMap = new HashSet<int>();
        var width = minesField.GetLength(0);
        var height = minesField.GetLength(1);
        for (var i = 0; i < height; i++)
        {
            for (var j = 0; j < width; j++)
            {
                if (minesField[j, i])
                {
                    var index = i * width + j;
                    minesMap.Add(index);
                }
            }
        }

        return new MinefieldConfig()
        {
            Width = width,
            Height = height,
            MinesMap = minesMap
        };
    }

    private int CreateSeed()
    {
        var newSeed = new Random().Next(999999999);
        return newSeed;
    }
}