using Minesweeper;
using Services.Services;

namespace MinesweeperASPNET.MinesweeperModelBridge;

public class MinesweeperTime : ITime
{
    private readonly ITimeService m_TimeService;

    public DateTime Now => m_TimeService.UtcNow;

    public MinesweeperTime(ITimeService timeService)
    {
        m_TimeService = timeService;
    }
}