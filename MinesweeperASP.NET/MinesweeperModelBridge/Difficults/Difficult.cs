﻿namespace MinesweeperASPNET.MinesweeperModelBridge.Difficults
{
    public static class Difficult
    {
        public const string Beginner = "Beginner";
        public const string Intermediate = "Intermediate";
        public const string Expert = "Expert";
    }
}