﻿namespace MinesweeperASPNET.MinesweeperModelBridge.Difficults;

public class DifficultMapHolder
{
    private readonly Dictionary<string, DifficultConfig> m_DifficultConfigsMap;

    public Dictionary<string, DifficultConfig> DifficultConfigsMap => m_DifficultConfigsMap;

    public DifficultMapHolder()
    {
        m_DifficultConfigsMap = new Dictionary<string, DifficultConfig>()
        {
            { Difficult.Beginner, new DifficultConfig(15, 10, 15) },
            { Difficult.Intermediate, new DifficultConfig(16, 16, 40) },
            { Difficult.Expert, new DifficultConfig(30, 16, 99) },
        };
    }
}