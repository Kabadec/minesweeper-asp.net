﻿namespace MinesweeperASPNET.MinesweeperModelBridge.Difficults
{
    public struct DifficultConfig
    {
        public int Width { get; }
        public int Height { get; }
        public int NumMines { get; }

        public DifficultConfig(int width, int height, int numMines)
        {
            Width = width;
            Height = height;
            NumMines = numMines;
        }
    }
}