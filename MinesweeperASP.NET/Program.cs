using Core;
using Services.Services;
using Tools;

namespace MinesweeperASPNET;

internal class Program
{
    public static void Main(string[] args)
    {
        var builder = WebApplication.CreateBuilder(args);
        IAppConstantsService appConstantsService = new AppConstantsService();
        var testMode = appConstantsService.IsTestMode;

        // var connection = builder.Configuration.GetConnectionString("DefaultConnection");
        Injections.AddServices(builder.Services, new AppSettings());
        
        if (testMode)
        {
            builder.Services.AddCors(c =>
            {
                c.AddDefaultPolicy(options =>
                {
                    options.SetIsOriginAllowed(_ => true)
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        .AllowCredentials();
                });
            });
        }

        builder.Services.AddAntiforgery();
        builder.Services.AddControllers();
        builder.Services.AddDistributedMemoryCache();
        builder.Services.AddSession(options =>
        {
            options.IdleTimeout = TimeSpan.FromMinutes(120);
            options.Cookie.IsEssential = true;
            options.Cookie.Name = appConstantsService.CookieSessionIdKey;
            options.Cookie.HttpOnly = false;
            if (testMode)
            {
                options.Cookie.SameSite = SameSiteMode.None;
                options.Cookie.SecurePolicy = CookieSecurePolicy.Always;
            }
        });
        builder.Services.AddMemoryCache();

        var app = builder.Build();

        if (!app.Environment.IsDevelopment())
        {
            app.UseExceptionHandler("/Error", createScopeForErrors: true);
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        app.UseStaticFiles();
        if (testMode)
        {
            app.UseCors();
        }

        app.UseHttpsRedirection();
        app.UseSession();
        app.UseAntiforgery();
        app.UseRouting();
        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllerRoute(
                name: "default",
                pattern: "{controller=Minesweeper}/{action=Index}"
            );
            endpoints.MapFallbackToFile("/", "/index.html");
        });

        app.Run();
    }
}