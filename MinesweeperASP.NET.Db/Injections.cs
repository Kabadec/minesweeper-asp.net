﻿using Db.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Tools;

namespace Db;

public static class Injections
{
    public static IServiceCollection AddDb(this IServiceCollection services, IAppSettings settings)
    {
        var connection = settings.DbConnectionString;
        services.AddDbContext<ApplicationContext>(options => options.UseLazyLoadingProxies().UseSqlServer(connection));
        
        return services;
    }
}
