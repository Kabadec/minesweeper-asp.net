namespace Db.Models;

public class Match
{
    public int Id { get; set; }
    public string SessionId { get; set; } = "";
    public string UserId { get; set; } = "";
    public DateTime TimeMatchStart { get; set; }
    public DateTime TimeMatchEnd { get; set; }
    public int MinesweeperState { get; set; }
    public byte[] Data { get; set; }
}