﻿using Microsoft.EntityFrameworkCore;

namespace Db.Models;

public class ApplicationContext : DbContext
{
    public DbSet<Match> MatchHistory { get; set; } = null!;

    public ApplicationContext(DbContextOptions<ApplicationContext> options) : base(options)
    {
    }
}