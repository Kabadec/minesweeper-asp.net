namespace Db.Interfaces;

public interface IDbConfigurator
{
    Task MigrateAsync();
}