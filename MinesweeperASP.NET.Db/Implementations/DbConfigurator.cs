using Db.Interfaces;
using Db.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Db.Implementations;

public class DbConfigurator : IDbConfigurator
{
    private readonly IServiceScopeFactory _factory;

    public DbConfigurator(IServiceScopeFactory factory)
    {
        _factory = factory;
    }


    #region IDbConfigurator

    async Task IDbConfigurator.MigrateAsync()
    {
        using (var scope = _factory.CreateScope())
        {
            await using var context = scope.ServiceProvider.GetService<ApplicationContext>();
            // var script = context.Database.GenerateCreateScript();
            // var text = 0;
            await context.Database.MigrateAsync();
            // await PostgresConfiguration.MigrateAsync(scope);
        }

        using (var scope = _factory.CreateScope())
        {
            // await MongoConfiguration.MigrateAsync(scope);
        }

        // using (var scope = _factory.CreateScope())
        // {portal_dev_kochergin_040624
        //     await ElasticConfiguration.MigrateAsync(scope);
        // }
// 
        // using (var scope = _factory.CreateScope())
        // {
        //     await RedisConfiguration.MigrateAsync(scope);
        // }
    }

    #endregion IDbConfigurator
}
