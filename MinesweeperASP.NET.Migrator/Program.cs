using Db.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace Migrator;

internal class Program
{
    static async Task Main(string[] args)
    {
        var builder = Host.CreateApplicationBuilder(args);
        var app = builder.Build();
        var db = app.Services.GetRequiredService<IDbConfigurator>();
        await db.MigrateAsync();
    }
}
